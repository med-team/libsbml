
Package: libsbml5-matlab
Architecture: any
Section: contrib/math
Depends: ${shlibs:Depends},
         ${misc:Depends}
Provides: libsbml-matlab
Description: System Biology Markup Language library - matlab bindings
 LibSBML is a library designed to help you read, write, manipulate,
 translate, and validate SBML files and data streams. It is not an
 application itself (though it does come with many example programs),
 but rather a library you can embed in your own applications.
 .
 This package contains the matlab bindings of LibSBML.
