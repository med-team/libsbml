#!/bin/sh
PYTH=$(basename $(python-config --includes | awk '{print $1}' | cut -dI -f2))
LINE=$(grep -n src/bindings/python/CMakeFiles/binding_python_lib.dir/depend build/CMakeFiles/Makefile2 | cut -d: -f1)

sed -i ""$LINE"i\\\tsed -i \"s#<Python.h>#<"$PYTH"/Python.h>#g\" src/bindings/python/libsbml_wrap.cpp" build/CMakeFiles/Makefile2
